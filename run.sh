#!/bin/bash

if [[ -z ${1} ]]; then
    echo "Usage: ./run.sh (q1 | q2 | q4 | q5)"
fi

mkdir -p build && cd build
cmake .. -DCMAKE_BUILD_TYPE="Release"

if [[ "${1}" == "q1" ]]; then
    make testrand
    ./testrand
fi

if [[ "${1}" == "q2" ]]; then
    make exe
    ./exe 2
fi

if [[ "${1}" == "q4" ]]; then
    make exe
    ./exe 4
fi

if [[ "${1}" == "q5" ]]; then
    make exe
    ./exe 5
fi
