#include "montecarlo.hpp"
#include "constants.hpp"
#include <future>
#include <numeric>
#include <vector>

typedef unsigned long long int ull;

double pi_estimation(CLHEP::MTwistEngine& rng, unsigned long long int iterations) {
    ull nb_points_inside = 0;

    for (ull i = 0; i < iterations * 2; i += 2) {
        auto x = rng.flat();
        auto y = rng.flat();
        if (x * x + y * y < 1) {
            ++nb_points_inside;
        }
    }

    return static_cast<double>(nb_points_inside) / static_cast<double>(iterations) * 4;
}

void generate_status(ull repetitions, ull range) {
    CLHEP::MTwistEngine rng{MT_SEED};

    for (ull i = 0; i < repetitions; ++i) {
        std::stringstream ss;
        ss << i << ".conf";
        rng.saveStatus(ss.str().c_str());
        for (ull j = 0; j < range; ++j) {
            rng.flat();
        }
    }
}

double parallel_pi(ull repetitions, ull range) {
    std::vector<std::future<double>> pi_estimations;
    pi_estimations.reserve(repetitions);
    for (ull repetition = 0; repetition < repetitions; ++repetition) {
        pi_estimations.push_back(std::async(std::launch::async, [repetition, range] {
            CLHEP::MTwistEngine rng{MT_SEED};
            std::stringstream ss;
            ss << repetition << ".conf";
            rng.restoreStatus(ss.str().c_str());

            return pi_estimation(rng, range);
        }));
    }

    return std::accumulate(std::begin(pi_estimations), std::end(pi_estimations), 0.0,
                           [](double acc, std::future<double>& f) { return acc + f.get(); })
           / static_cast<double>(repetitions);
}
